DROP TRIGGER IF EXISTS account_deposit;
DROP TRIGGER IF EXISTS account_withdraw;
DELIMITER $$
CREATE TRIGGER account_deposit
	AFtER INSERT ON Deposit
	FOR EACH ROW
BEGIN 
	 IF(New.Amount > 0)THEN
	UPDATE Account SET 
	Balance = Balance + New.Amount
	WHERE ACC_No =New.ACC_No;
END IF;
END $$


CREATE TRIGGER account_withdraw
	AFtER INSERT ON Withdraw
	FOR EACH ROW
BEGIN 
	 IF(New.Amount > 0)THEN

	UPDATE Account SET 
	Balance = Balance - New.Amount
	WHERE ACC_No = New.ACC_No;
END IF;
END $$

DELIMITER ;

