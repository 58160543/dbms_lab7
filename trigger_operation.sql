DROP TRIGGER IF EXISTS account_operation;

DELIMITER $$
CREATE TRIGGER account_operation
	AFTER INSERT ON Operation
	FOR EACH ROW
BEGIN
	IF(New.Action='D')THEN
		IF(New.Amount >0)THEN
		UPDATE Account
	SET Balance = Balance + New.Amount
	WHERE ACC_No =New.ACC_No_Source;
	END IF;
	
	ELSEIF(New.Action='W')THEN
		IF(New.Amount >0)THEN
		UPDATE Account
	SET Balance = Balance - New.Amount
	WHERE ACC_No =New.ACC_No_Source;
	END IF;
	
	ELSEIF(New.Action='T')THEN
		IF(New.Amount >0)THEN
		UPDATE Account
	SET Balance = Balance - New.Amount
	WHERE ACC_No =New.ACC_No_Source;
	UPDATE Account 
	SET Balance = Balance +New.Amount
	WHERE ACC_No =New.ACC_No_Dest;
		END IF;
	END IF;
	
END $$
DELIMITER ;


