DROP TRIGGER IF EXISTS account_transfer;

DELIMITER $$

CREATE TRIGGER account_transfer
	AFTER INSERT ON Transfer
	FOR EACH ROW
BEGIN
	IF (NEW.Amount > 0) THEN
	UPDATE Account SET 
	Balance = Balance - New.Amount 
	WHERE ACC_No = ACC_NO_Source;

	UPDATE Account SET 
	Balance = Balance + New.Amount
	WHERE ACC_No = New.ACC_No_Dest;	
END IF;
END $$

DELIMITER $$
